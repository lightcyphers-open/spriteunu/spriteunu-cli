# spriteunu-cli

A command-line interface to [spriteunu](https://gitlab.com/lightcyphers-open/spriteunu).

## Installation

    yarn global add @lightcyphers/spriteunu-cli

## Usage

    spriteunu [output filename] [input directory]

      --retina      shorthand for --ratio=2
      --ratio=[n]   pixel ratio
      --unique      map identical images to multiple names

Spriteunu reads an input directory containing SVG files and generates a JSON
layout file and PNG spritesheet.